/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sic.gov.co.tramites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ingeniun
 */
@Entity
@Table(name = "SIC_PERSONA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SicPersona.findAll", query = "SELECT s FROM SicPersona s"),
    @NamedQuery(name = "SicPersona.findByIdPersona", query = "SELECT s FROM SicPersona s WHERE s.idPersona = :idPersona"),
    @NamedQuery(name = "SicPersona.findByTipoIdentificacion", query = "SELECT s FROM SicPersona s WHERE s.tipoIdentificacion = :tipoIdentificacion"),
    @NamedQuery(name = "SicPersona.findByNumeroIdentificacion", query = "SELECT s FROM SicPersona s WHERE s.numeroIdentificacion = :numeroIdentificacion"),
    @NamedQuery(name = "SicPersona.findByNombres", query = "SELECT s FROM SicPersona s WHERE s.nombres = :nombres"),
    @NamedQuery(name = "SicPersona.findByApellidos", query = "SELECT s FROM SicPersona s WHERE s.apellidos = :apellidos"),
    @NamedQuery(name = "SicPersona.findByTelefono", query = "SELECT s FROM SicPersona s WHERE s.telefono = :telefono"),
    @NamedQuery(name = "SicPersona.findByDireccion", query = "SELECT s FROM SicPersona s WHERE s.direccion = :direccion"),
    @NamedQuery(name = "SicPersona.findByEmail", query = "SELECT s FROM SicPersona s WHERE s.email = :email")})
public class SicPersona implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_PERSONA")
    private BigDecimal idPersona;
    @Column(name = "TIPO_IDENTIFICACION")
    private String tipoIdentificacion;
    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "APELLIDOS")
    private String apellidos;
    @Column(name = "TELEFONO")
    private Long telefono;
    @Column(name = "DIRECCION")
    private String direccion;
    @Column(name = "EMAIL")
    private String email;
    @JoinColumn(name = "SIC_EMPREADOS_ID_EMPLEADO", referencedColumnName = "ID_EMPLEADO")
    @ManyToOne
    private SicEmpreados sicEmpreadosIdEmpleado;
    @OneToMany(mappedBy = "sicPersonaIdPersona")
    private List<SicTramitePersona> sicTramitePersonaList;

    public SicPersona() {
    }

    public SicPersona(BigDecimal idPersona) {
        this.idPersona = idPersona;
    }

    public BigDecimal getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(BigDecimal idPersona) {
        this.idPersona = idPersona;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public SicEmpreados getSicEmpreadosIdEmpleado() {
        return sicEmpreadosIdEmpleado;
    }

    public void setSicEmpreadosIdEmpleado(SicEmpreados sicEmpreadosIdEmpleado) {
        this.sicEmpreadosIdEmpleado = sicEmpreadosIdEmpleado;
    }

    @XmlTransient
    public List<SicTramitePersona> getSicTramitePersonaList() {
        return sicTramitePersonaList;
    }

    public void setSicTramitePersonaList(List<SicTramitePersona> sicTramitePersonaList) {
        this.sicTramitePersonaList = sicTramitePersonaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersona != null ? idPersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SicPersona)) {
            return false;
        }
        SicPersona other = (SicPersona) object;
        if ((this.idPersona == null && other.idPersona != null) || (this.idPersona != null && !this.idPersona.equals(other.idPersona))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sic.gov.co.sic.tramites.SicPersona[ idPersona=" + idPersona + " ]";
    }
    
}
