/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sic.gov.co.tramites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ingeniun
 */
@Entity
@Table(name = "SIC_EMPREADOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SicEmpreados.findAll", query = "SELECT s FROM SicEmpreados s"),
    @NamedQuery(name = "SicEmpreados.findByIdEmpleado", query = "SELECT s FROM SicEmpreados s WHERE s.idEmpleado = :idEmpleado"),
    @NamedQuery(name = "SicEmpreados.findByIdDependencia", query = "SELECT s FROM SicEmpreados s WHERE s.idDependencia = :idDependencia"),
    @NamedQuery(name = "SicEmpreados.findByFFechaIngreso", query = "SELECT s FROM SicEmpreados s WHERE s.fFechaIngreso = :fFechaIngreso"),
    @NamedQuery(name = "SicEmpreados.findByIdUsuario", query = "SELECT s FROM SicEmpreados s WHERE s.idUsuario = :idUsuario"),
    @NamedQuery(name = "SicEmpreados.loginUsuario", query = "SELECT s FROM SicEmpreados s WHERE s.idUsuario = :idUsuario AND s.contrasena = :contrasena"),
    @NamedQuery(name = "SicEmpreados.findByContrasena", query = "SELECT s FROM SicEmpreados s WHERE s.contrasena = :contrasena")})
public class SicEmpreados implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_EMPLEADO")
    private BigDecimal idEmpleado;
    @Column(name = "ID_DEPENDENCIA")
    private String idDependencia;
    @Column(name = "F_FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fFechaIngreso;
    @Column(name = "ID_USUARIO")
    private String idUsuario;
    @Column(name = "CONTRASENA")
    private String contrasena;
    @OneToMany(mappedBy = "sicEmpreadosIdEmpleado")
    private List<SicPersona> sicPersonaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sicEmpreadosIdEmpleado")
    private List<SicTramitePersona> sicTramitePersonaList;

    public SicEmpreados() {
    }

    public SicEmpreados(BigDecimal idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public BigDecimal getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(BigDecimal idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getIdDependencia() {
        return idDependencia;
    }

    public void setIdDependencia(String idDependencia) {
        this.idDependencia = idDependencia;
    }

    public Date getFFechaIngreso() {
        return fFechaIngreso;
    }

    public void setFFechaIngreso(Date fFechaIngreso) {
        this.fFechaIngreso = fFechaIngreso;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @XmlTransient
    public List<SicPersona> getSicPersonaList() {
        return sicPersonaList;
    }

    public void setSicPersonaList(List<SicPersona> sicPersonaList) {
        this.sicPersonaList = sicPersonaList;
    }

    @XmlTransient
    public List<SicTramitePersona> getSicTramitePersonaList() {
        return sicTramitePersonaList;
    }

    public void setSicTramitePersonaList(List<SicTramitePersona> sicTramitePersonaList) {
        this.sicTramitePersonaList = sicTramitePersonaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpleado != null ? idEmpleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SicEmpreados)) {
            return false;
        }
        SicEmpreados other = (SicEmpreados) object;
        if ((this.idEmpleado == null && other.idEmpleado != null) || (this.idEmpleado != null && !this.idEmpleado.equals(other.idEmpleado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sic.gov.co.sic.tramites.SicEmpreados[ idEmpleado=" + idEmpleado + " ]";
    }
    
}
