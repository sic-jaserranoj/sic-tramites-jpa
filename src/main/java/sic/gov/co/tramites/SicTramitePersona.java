/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sic.gov.co.tramites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ingeniun
 */
@Entity
@Table(name = "SIC_TRAMITE_PERSONA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SicTramitePersona.findAll", query = "SELECT s FROM SicTramitePersona s"),
    @NamedQuery(name = "SicTramitePersona.findByIdTramitePersona", query = "SELECT s FROM SicTramitePersona s WHERE s.idTramitePersona = :idTramitePersona"),
    @NamedQuery(name = "SicTramitePersona.findByNumeroRadicado", query = "SELECT s FROM SicTramitePersona s WHERE s.numeroRadicado = :numeroRadicado"),
    @NamedQuery(name = "SicTramitePersona.findByFechaRadicado", query = "SELECT s FROM SicTramitePersona s WHERE s.fechaRadicado = :fechaRadicado")})
public class SicTramitePersona implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE_PERSONA")
    private BigDecimal idTramitePersona;
    @Column(name = "NUMERO_RADICADO")
    private String numeroRadicado;
    @Column(name = "FECHA_RADICADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRadicado;
    @JoinColumn(name = "SIC_EMPREADOS_ID_EMPLEADO", referencedColumnName = "ID_EMPLEADO")
    @ManyToOne(optional = false)
    private SicEmpreados sicEmpreadosIdEmpleado;
    @JoinColumn(name = "SIC_PERSONA_ID_PERSONA", referencedColumnName = "ID_PERSONA")
    @ManyToOne
    private SicPersona sicPersonaIdPersona;
    @JoinColumn(name = "SIC_TRAMITE_ID_TRAMITE", referencedColumnName = "ID_TRAMITE")
    @ManyToOne(optional = false)
    private SicTramite sicTramiteIdTramite;

    public SicTramitePersona() {
    }

    public SicTramitePersona(BigDecimal idTramitePersona) {
        this.idTramitePersona = idTramitePersona;
    }

    public BigDecimal getIdTramitePersona() {
        return idTramitePersona;
    }

    public void setIdTramitePersona(BigDecimal idTramitePersona) {
        this.idTramitePersona = idTramitePersona;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public Date getFechaRadicado() {
        return fechaRadicado;
    }

    public void setFechaRadicado(Date fechaRadicado) {
        this.fechaRadicado = fechaRadicado;
    }

    public SicEmpreados getSicEmpreadosIdEmpleado() {
        return sicEmpreadosIdEmpleado;
    }

    public void setSicEmpreadosIdEmpleado(SicEmpreados sicEmpreadosIdEmpleado) {
        this.sicEmpreadosIdEmpleado = sicEmpreadosIdEmpleado;
    }

    public SicPersona getSicPersonaIdPersona() {
        return sicPersonaIdPersona;
    }

    public void setSicPersonaIdPersona(SicPersona sicPersonaIdPersona) {
        this.sicPersonaIdPersona = sicPersonaIdPersona;
    }

    public SicTramite getSicTramiteIdTramite() {
        return sicTramiteIdTramite;
    }

    public void setSicTramiteIdTramite(SicTramite sicTramiteIdTramite) {
        this.sicTramiteIdTramite = sicTramiteIdTramite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTramitePersona != null ? idTramitePersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SicTramitePersona)) {
            return false;
        }
        SicTramitePersona other = (SicTramitePersona) object;
        if ((this.idTramitePersona == null && other.idTramitePersona != null) || (this.idTramitePersona != null && !this.idTramitePersona.equals(other.idTramitePersona))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sic.gov.co.sic.tramites.SicTramitePersona[ idTramitePersona=" + idTramitePersona + " ]";
    }
    
}
