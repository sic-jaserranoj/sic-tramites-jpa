/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sic.gov.co.tramites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ingeniun
 */
@Entity
@Table(name = "SIC_TRAMITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SicTramite.findAll", query = "SELECT s FROM SicTramite s"),
    @NamedQuery(name = "SicTramite.findByIdTramite", query = "SELECT s FROM SicTramite s WHERE s.idTramite = :idTramite"),
    @NamedQuery(name = "SicTramite.findByTipoTramite", query = "SELECT s FROM SicTramite s WHERE s.tipoTramite = :tipoTramite"),
    @NamedQuery(name = "SicTramite.findByInfoTramite", query = "SELECT s FROM SicTramite s WHERE s.infoTramite = :infoTramite")})
public class SicTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE")
    private BigDecimal idTramite;
    @Column(name = "TIPO_TRAMITE")
    private String tipoTramite;
    @Column(name = "INFO_TRAMITE")
    private String infoTramite;
    @OneToMany(mappedBy = "sicTramiteIdTramite")
   private List<SicTramitePersona> sicTramitePersonaList;

    public SicTramite() {
    }

    public SicTramite(BigDecimal idTramite) {
        this.idTramite = idTramite;
    }

    public BigDecimal getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(BigDecimal idTramite) {
        this.idTramite = idTramite;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getInfoTramite() {
        return infoTramite;
    }

    public void setInfoTramite(String infoTramite) {
        this.infoTramite = infoTramite;
    }

    @XmlTransient
    public List<SicTramitePersona> getSicTramitePersonaList() {
        return sicTramitePersonaList;
    }

    public void setSicTramitePersonaList(List<SicTramitePersona> sicTramitePersonaList) {
        this.sicTramitePersonaList = sicTramitePersonaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTramite != null ? idTramite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SicTramite)) {
            return false;
        }
        SicTramite other = (SicTramite) object;
        if ((this.idTramite == null && other.idTramite != null) || (this.idTramite != null && !this.idTramite.equals(other.idTramite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sic.gov.co.sic.tramites.SicTramite[ idTramite=" + idTramite + " ]";
    }
    
}
